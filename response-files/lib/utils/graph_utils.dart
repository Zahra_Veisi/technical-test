import 'dart:math';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:task_project/models/chart_item_model.dart';
import 'package:task_project/models/graph_model.dart';

//Build line charts
buildLineChartData(GraphModel points, BuildContext context) {
  return LineChartData(
    backgroundColor: Theme.of(context).scaffoldBackgroundColor,
    lineTouchData: LineTouchData(
      touchTooltipData: LineTouchTooltipData(
        tooltipRoundedRadius: 10,
        tooltipPadding: EdgeInsets.all(8.0),
        getTooltipItems: (List<LineBarSpot> lineBarsSpot) {
          return lineBarsSpot.map((lineBarSpot) {
            return LineTooltipItem(
              lineBarSpot.y.toString(),
              TextStyle(
                  color: Colors.blue[600],
                  fontWeight: FontWeight.bold,
                  fontSize: 12.0),
            );
          }).toList();
        },
      ),
    ),
    titlesData: FlTitlesData(
      show: true,
      bottomTitles: SideTitles(
        showTitles: true,
        reservedSize: 22,
        textStyle: TextStyle(color: Colors.white, fontSize: 11),
        getTitles: (value) {
          if (points.xPoints.length <= 20) return value.toStringAsFixed(0);
          return '';
        },
        margin: 0,
      ),
      leftTitles: SideTitles(
        showTitles: false,
        textStyle: TextStyle(
          color: Colors.white,
          fontSize: 10,
        ),
        getTitles: (value) {
          return value.toInt().toString();
        },
        margin: 18,
        reservedSize: 0,
      ),
    ),
    borderData: FlBorderData(
        show: false, border: Border.all(color: Colors.white, width: 1)),
    minY: getMinPointInThreeLists(
            points.xPoints, points.yPoints, points.zPoints) -
        0.02,
    maxY: getMaxPointInThreeLists(
            points.xPoints, points.yPoints, points.zPoints) +
        0.02,
    minX: 0,
    maxX: points.xPoints.length.toDouble() - 1,
    axisTitleData: FlAxisTitleData(
      show: true,
      bottomTitle: AxisTitle(
          titleText: 'Time Index',
          showTitle: true,
          textStyle: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: MediaQuery.of(context).size.width * 0.04),
          textAlign: TextAlign.center),
    ),
    lineBarsData: [
      LineChartBarData(
        spots: points.xPoints,
        isCurved: true,
        colors: [Color(0xff1c94cb)],
        barWidth: 3,
        isStrokeCapRound: true,
        dotData: FlDotData(
          show: false,
        ),
      ),
      LineChartBarData(
        spots: points.yPoints,
        isCurved: true,
        colors: [Colors.red],
        barWidth: 3,
        isStrokeCapRound: true,
        dotData: FlDotData(
          show: false,
        ),
      ),
      LineChartBarData(
        spots: points.zPoints,
        isCurved: true,
        colors: [Colors.green],
        barWidth: 3,
        isStrokeCapRound: true,
        dotData: FlDotData(
          show: false,
        ),
      )
    ],
  );
}

//Gets the min Y value in three lists
double getMinPointInThreeLists(
    List<FlSpot> xPoints, List<FlSpot> yPoints, List<FlSpot> zPoints) {
  List<double> xResult = List();
  List<double> yResult = List();
  List<double> zResult = List();
  for (int i = 0; i < xPoints.length; i++) {
    xResult.add(xPoints[i].y);
    yResult.add(yPoints[i].y);
    zResult.add(zPoints[i].y);
  }
  double xMax = xResult.reduce((min));
  double yMax = yResult.reduce((min));
  double zMax = zResult.reduce((min));
  List<double> result = [xMax, yMax, zMax];
  return result.reduce((min));
}

//Gets the max Y value in three lists
double getMaxPointInThreeLists(
    List<FlSpot> xPoints, List<FlSpot> yPoints, List<FlSpot> zPoints) {
  List<double> xResult = List();
  List<double> yResult = List();
  List<double> zResult = List();
  for (int i = 0; i < xPoints.length; i++) {
    xResult.add(xPoints[i].y);
    yResult.add(yPoints[i].y);
    zResult.add(zPoints[i].y);
  }
  double xMax = xResult.reduce((max));
  double yMax = yResult.reduce((max));
  double zMax = zResult.reduce((max));
  List<double> result = [xMax, yMax, zMax];
  return result.reduce((max));
}

//Converts list of maps from the API to a type that's supported by the charts library
GraphModel prepareXYZPointsFromModel(List data) {
  if (data == null) return null;
  List<FlSpot> xPoints = List();
  List<FlSpot> yPoints = List();
  List<FlSpot> zPoints = List();
  for (int i = 0; i < data.length; i++) {
    ChartItemModel chartItemModel = ChartItemModel.fromMap(data[i]);
    DateTime dt = DateTime.parse(chartItemModel.datetime);

    xPoints.add(
        FlSpot(i.toDouble(), double.parse(chartItemModel.xAcc.toString())));
    yPoints.add(
        FlSpot(i.toDouble(), double.parse(chartItemModel.yAcc.toString())));
    zPoints.add(
        FlSpot(i.toDouble(), double.parse(chartItemModel.zAcc.toString())));
  }
  return GraphModel(xPoints, yPoints, zPoints);
}
