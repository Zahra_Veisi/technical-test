import 'package:flutter/foundation.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:task_project/models/graph_model.dart';
import 'package:task_project/models/main_api_response_model.dart';
import 'package:task_project/repo/api_repo.dart';
import 'package:task_project/utils/app_utils.dart';
import 'package:task_project/utils/graph_utils.dart';

class ChartProvider with ChangeNotifier {
  bool _showLoading;
  List chartItems;
  GraphModel _graphModel;
  List<LatLng> _mapLocationsList;
  String _errorMessage = '';

  GraphModel get graphModel => _graphModel;

  String get errorMessage => _errorMessage;

  List<LatLng> get mapLocationsList => _mapLocationsList;

  bool get showLoading => _showLoading;

  ChartProvider() {
    _showLoading = false;
    chartItems = List();
    getChartDataFromAPI();
  }

  _setShowLoading(bool load) {
    _showLoading = load;
    notifyListeners();
  }

  getChartDataFromAPI() async {
    _errorMessage = '';
    _setShowLoading(true);
    MainApiResponseModel responseModel = await ApiRepository.getPointsFromAPI();
    _setShowLoading(false);
    if (responseModel != null) {
      if (responseModel.data != null) {
        if (responseModel.data.isNotEmpty) {
          chartItems = responseModel.data;
          List temp = chartItems;
          if (chartItems.length > 10) {
            temp = List();
            for (int i = 0; i < 10; i++) {
              temp.add(chartItems);
            }
          }
          _graphModel = prepareXYZPointsFromModel(temp);
          _mapLocationsList = getLatLngFromAPIResponseList(responseModel.data);
          notifyListeners();
        }
      }
    } else {
      _errorMessage =
          'Some error happened while getting the data. Please refresh again.';
      notifyListeners();
    }
  }
}
