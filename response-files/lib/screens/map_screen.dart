import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapScreen extends StatefulWidget {
  final List mapLocationsList;
  MapScreen(this.mapLocationsList);
  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  Completer<GoogleMapController> _controller = Completer();
  final Set<Polyline> _polyline = {};
  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(0, 0),
    zoom: 14.00,
  );

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: SafeArea(
        child: GoogleMap(
          initialCameraPosition: _kGooglePlex,
          polylines: _polyline,
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
            setState(() {
              _polyline.add(Polyline(
                polylineId: PolylineId(widget.mapLocationsList.toString()),
                visible: true,
                points: widget.mapLocationsList,
                color: Colors.blue,
              ));
            });
            _goToTarget(widget.mapLocationsList.first);
          },
        ),
      ),
    );
  }

  Future<void> _goToTarget(LatLng latLng) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        bearing: 180.0, target: latLng, tilt: 60.0, zoom: 20.0)));
  }
}
