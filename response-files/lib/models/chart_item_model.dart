//A class that represents a point on the chart

class ChartItemModel {
  var datetime;
  var lat;
  var long;
  var xAcc;
  var yAcc;
  var zAcc;

  ChartItemModel(
      {this.datetime, this.lat, this.long, this.xAcc, this.yAcc, this.zAcc});

  factory ChartItemModel.fromMap(Map item) {
    if (item == null) return ChartItemModel.emptyResponse();
    return ChartItemModel(
      datetime: item['datetime'],
      lat: item['lat'],
      long: item['lon'],
      xAcc: item['xAcc'],
      yAcc: item['yAcc'],
      zAcc: item['zAcc'],
    );
  }

  factory ChartItemModel.emptyResponse() => ChartItemModel(
        datetime: '',
        lat: '',
        long: '',
        xAcc: '',
        yAcc: '',
        zAcc: '',
      );
}
