import 'package:fl_chart/fl_chart.dart';

class GraphModel {
  final List<FlSpot> _xPoints;
  final List<FlSpot> _yPoints;
  final List<FlSpot> _zPoints;

  List<FlSpot> get xPoints => _xPoints;

  List<FlSpot> get yPoints => _yPoints;

  List<FlSpot> get zPoints => _zPoints;

  GraphModel(this._xPoints, this._yPoints, this._zPoints);
}
