import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:task_project/providers/chart_provider.dart';
import 'package:task_project/screens/map_screen.dart';
import 'package:task_project/utils/graph_utils.dart';
import 'package:task_project/reusable_widgets/legend.dart';

class ChartScreen extends StatefulWidget {
  @override
  _ChartScreenState createState() => _ChartScreenState();
}

class _ChartScreenState extends State<ChartScreen> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => ChartProvider(),
      child: Consumer<ChartProvider>(builder: (context, snapshot, _) {
        return Scaffold(
          appBar: AppBar(
            title: Text('Chart'),
          ),
          body: SafeArea(
              child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              snapshot.chartItems != null &&
                      !snapshot.showLoading &&
                      snapshot.errorMessage.isEmpty
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal:
                                  MediaQuery.of(context).size.width * 0.06),
                          child: Column(
                            children: <Widget>[
                              LegendIndicator(
                                color: Colors.blue,
                                text: 'xAcc',
                              ),
                              SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01,
                              ),
                              LegendIndicator(
                                color: Colors.red,
                                text: 'yAcc',
                              ),
                              SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01,
                              ),
                              LegendIndicator(
                                color: Colors.green,
                                text: 'zAcc',
                              ),
                            ],
                          ),
                        ),
                        LineChart(
                            buildLineChartData(snapshot.graphModel, context))
                      ],
                    )
                  : Container(),
              Visibility(
                visible: !snapshot.showLoading && snapshot.errorMessage.isEmpty,
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height * 0.04),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: FlatButton(
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) =>
                                MapScreen(snapshot.mapLocationsList)));
                      },
                      color: Colors.lightBlue,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                      child: Text(
                        'Show Path',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: MediaQuery.of(context).size.width * 0.04),
                      ),
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: snapshot.errorMessage != '',
                child: Center(
                    child: Text(
                  snapshot.errorMessage ?? '',
                  style: TextStyle(
                      fontSize: MediaQuery.of(context).size.width * 0.04),
                  textAlign: TextAlign.center,
                )),
              ),
              Visibility(
                visible: snapshot.showLoading,
                child: SpinKitFadingCircle(
                  color: Colors.grey,
                ),
              )
            ],
          )),
        );
      }),
    );
  }
}
