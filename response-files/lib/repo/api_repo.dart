//2 methods for requesting data, either can be used
import 'package:http/http.dart' as http;
import 'package:task_project/constants/constants.dart';
import 'package:task_project/models/main_api_response_model.dart';
import 'dart:async';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:retry/retry.dart';

class ApiRepository {
  static Future<MainApiResponseModel> getPointsFromAPI() async {
    //--------------------------------------------------------------------------
    //Method 1, using 'dio' and 'retry' package
    final Dio _dio = Dio();
    Response response = await retry(
      () => _dio
          .get(APIEndPoints.kChartsAPIEndPoint)
          .timeout(Duration(seconds: 4)),
      retryIf: (error) =>
          error is SocketException ||
          error is TimeoutException ||
          error is HttpException ||
          error is DioError,
    );

    String result = response.toString();

    return MainApiResponseModel.fromJson(result);

    //--------------------------------------------------------------------------
    //Method 2, using 'http' and 'while loop' for retry
    // while (true) {
    //   http.Response response = await http.get(APIEndPoints.kChartsAPIEndPoint);
    //   if (response.statusCode == 200) {
    //     String result = response.body;
    //     if (result.isNotEmpty) return MainApiResponseModel.fromJson(result);
    //     break;
    //   }
    // }
  }
}
