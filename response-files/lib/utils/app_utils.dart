import 'package:google_maps_flutter/google_maps_flutter.dart';

//Get LatLng
List<LatLng> getLatLngFromAPIResponseList(List data) {
  if (data == null) return [];
  List<LatLng> result = List();
  for (int i = 0; i < data.length; i++) {
    if (data[i]['lat'] == null || data[i]['lon'] == null) continue;
    try {
      LatLng latLng = LatLng(double.parse(data[i]['lat'].toString()),
          double.parse(data[i]['lon'].toString()));
      result.add(latLng);
    } catch (e) {
      print('Error parsing lat or long from the list $e');
    }
  }
  return result;
}
