//A class that models the data coming from the main charts API.

import 'dart:convert';
import 'package:flutter/foundation.dart';

class MainApiResponseModel {
  List data;
  var hasMore;

  MainApiResponseModel({@required this.data, this.hasMore});

  factory MainApiResponseModel.fromJson(String json) {
    if (json == null) return MainApiResponseModel.emptyResponse();
    var decodedJson = jsonDecode(json);
    if (decodedJson != null) {
      return MainApiResponseModel(
          data: decodedJson['data'], hasMore: decodedJson['hasMore']);
    }
    return MainApiResponseModel.emptyResponse();
  }

  factory MainApiResponseModel.emptyResponse() =>
      MainApiResponseModel(data: [], hasMore: false);
}
